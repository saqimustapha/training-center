# Training-center
*** 
Conception d'une base de données et des requêtes JDBC pour une application de gestion d'un centre de formation.
Celle-ci prévoit de gérer de cursus de recrutement pour les sessions de formations.

## Présentation
* Use Case 
* Diagrame de Class
* Base de données
* Java

### Use Case
*** 
Pour la création du Use Case, nous avons utlisé le logiciel "StartUML". 
<img src="documentation/UseCase.png" alt="Use Case" width="720">

### Diagrame de Class
*** 
Pour la création du Diagramme de Class, nous avons utlisé le logiciel "StartUML".
<img src="documentation/ClassDiagram.png" alt="Class Diagram" width="720">

### Structure de la base de données : 
*** 
La base de données s'articule autour des tables suivantes :
* formation             : Formations pouvant être proposées
* session               : Différentes sessions de formation
* person                : Toutes les personnes qui seront différencié par leur rôle
* role                  : Différents rôles (Candidat, Apprenant, Formateur, Responsable Formation)
* session_person_joint  : Table de jointure pour stocker les formateurs assignés à une session de formation
* candidacy             : Candidatures sur les différentes sessions
* status                : Différents statuts pouvant être assignés à une candidature
* commentary            : Commentaires concernant les candidatures pouvant être donnéés par les personnes du centre de formation.

La struture et les données de la base de données peuvent être importées en exécutant le script "scripts_sql/b_creation_script.sql

### Programme Java : 
***

#### Entity
On retrouve dans ce package :
* la déclaration des différentes class fonctionnelles de l'application
* les méthodes associées

Par exemple, on retrouve dans session, une methode qui peut être appelé pour tester si les données sont valide avant d'être envoyées vers la base de données.

```
public boolean isValid() {
    boolean valid = false;
    if (this.training != null && endDate.compareTo(startDate) > 0 && referent != null) {
      valid = true;
    }
    return valid;
  }
```

#### Repository
On retrouve dans ce package :
* la déclaration des différentes class pour effectuer les requêtes JDBC
* les méthodes associées

Nous avons créé pour toutes les tables les méthodes suivantes :
* findAll       : Retourne la liste de tous les éléments
* findById      : Retourne l'élément dont l'id correspond à celui passé en paramètre
* save          : Crée une nouvelle ligne à partir des données passé en entrée
* update        : Met à jour les données d'une ligne avec les nouvelles valeurs
* deleteById    : Supprime la ligne dont l'id correspond à celui passé en paramètre

D'autres méthodes ont été créées pour répondre à des besoins plus spécifique de l'application.
Par exemple, on retrouve dans person, une méthode pour récupérer le rôle d'une personne.

```
public void updateRole(Person person) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT role.id, role.label FROM role INNER JOIN person ON person.id_role = role.id WHERE person.id=?");

      stmt.setInt(1, person.getId());

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        person.setRole(new Role(rs.getInt("id"),rs.getString("label")));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
```

## Piste d'amélioration
***
Nous n'avons pas pu réaliser toutes les fonctionnalités que nous voulions, par exemple :
* la table 'Commentary' a été créé avec une clé primaire multiple, ce qui ne nous permet pas de conserver le commentaire si une personne est supprimé. Il aurait fallu faire une clé primaire simple et mettre le verrouillage dans Java pour qu'une personne ne puisse pas déposer plusieurs commentaires sur une même candidature.
* il manque des méthodes de contrôle ou de gestion de l'application


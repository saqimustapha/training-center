package co.simplon.promo18;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import co.simplon.promo18.entity.Role;
import co.simplon.promo18.entity.Candidacy;
import co.simplon.promo18.entity.Person;
import co.simplon.promo18.entity.Session;
import co.simplon.promo18.entity.Training;
import co.simplon.promo18.entity.Status;
import co.simplon.promo18.repository.CandidacyRepository;
import co.simplon.promo18.repository.PersonRepository;
import co.simplon.promo18.repository.RoleRepository;
import co.simplon.promo18.repository.SessionRepository;
import co.simplon.promo18.repository.StatusRepository;
import co.simplon.promo18.repository.TrainingRepository;

/**
 *
 *
 */
public class App {
  public static void main(String[] args) {
    int choix = 0;

    Scanner sc = new Scanner(System.in);

    CandidacyRepository candidacyRepo = new CandidacyRepository();
    PersonRepository personRepo = new PersonRepository();
    RoleRepository roleRepo = new RoleRepository();
    SessionRepository sessionRepo = new SessionRepository();
    StatusRepository statusRepo = new StatusRepository();
    TrainingRepository trainingRepo = new TrainingRepository();

    List<Person> listPerson = new ArrayList<>();

    // Person
    // System.out.println(personRepo.findById(5));
    // personRepo.save(new Person(new Role(1), "Dastous", "Roger", LocalDate.parse("1999-12-12"),
    // "+33 4 32 58 39 18", "rogerrastous@dayrep.com ", "84", "boulevard d Alsace", "69200",
    // "Vénissieux", "France"));
    // personRepo.update(new Person(19, new Role(2), "Dastous", "Roger",
    // LocalDate.parse("1999-12-12"),
    // "+33 4 32 58 39 18", "rogerrastous@dayrep.com ", "84", "boulevard d Alsace", "69200",
    // "Vénissieux", "France"));
    // personRepo.deleteById(19);

    // Role
    // System.out.println(roleRepo.findById(2));
    // roleRepo.save(new Role("DRH"));
    // roleRepo.update(new Role(5,"Comptable"));
    // roleRepo.deleteById(5);

    // Status
    // System.out.println(statusRepo.findById(2));
    // statusRepo.save(new Status("Statut vide"));
    // statusRepo.update(new Status(6,"Annulée"));
    // statusRepo.deleteById(6);
    // System.out.println(trainingRepo.findById(2));

    // session
    // System.out.println(sessionRepo.findById(2));
    // Person ref = personRepo.findById(15);
    // Training tr = trainingRepo.findById(2);
    // // sessionRepo
    // //     .save(new Session(tr, LocalDate.parse("2022-06-11"), LocalDate.parse("2022-12-16"),ref));
    // sessionRepo.update(
    //     new Session(4, tr, LocalDate.parse("2022-06-11"), LocalDate.parse("2022-12-16"), ref));
    // sessionRepo.deleteById(4);

    // candidacy
    // System.out.println(candidacyRepo.findById(9));
    // candidacyRepo.save(new Candidacy(2, 2, LocalDate.parse("2022-02-23"), "app_form_link_10",
    // "cv_link_32", "cover_letter_link_40", 2));
    // candidacyRepo.update(new Candidacy(16, 2, 2, LocalDate.parse("2022-02-23"),
    // "app_form_link_10",
    // "cv_link_18", "cover_letter_link_33", 4));
    // candidacyRepo.deleteById(16);

    while (choix != 99) {
      System.out.println("Afficher Table:");
      System.out.println("1 = Candidacy");
      System.out.println("2 = Person");
      System.out.println("3 = Role");
      System.out.println("4 = Session");
      System.out.println("5 = Status");
      System.out.println("6 = Training");

      choix = sc.nextInt();

      switch (choix) {
        case 1:
          List<Candidacy> listCandidacy = candidacyRepo.findAll();
          for (Candidacy candidacy : listCandidacy) {
            System.out.println(candidacy);
          }
          break;
        case 2:
          listPerson = personRepo.findAll();
          // listPerson = personRepo.findByRole(2);
          for (Person person : listPerson) {
            System.out.println(person);
          }
          break;
        case 3:
          List<Role> listRole = roleRepo.findAll();
          for (Role role : listRole) {
            System.out.println(role);
          }
          break;
        case 4:
          List<Session> listSession = sessionRepo.findAll();
          for (Session session : listSession) {
            System.out.println(session);
          }
          break;

        case 5:
          List<Status> listStatus = statusRepo.findAll();
          for (Status status : listStatus) {
            System.out.println(status);
          }
          break;
        case 6:
          List<Training> listTraining = trainingRepo.findAll();
          for (Training training : listTraining) {
            System.out.println(training);
          }
          break;
        case 7:
          if (listPerson.get(5) == null) {
            personRepo.updateRole(listPerson.get(5));
          }
          break;
        default:
          choix = 99;
          break;
      }
    }
    sc.close();
  }

}

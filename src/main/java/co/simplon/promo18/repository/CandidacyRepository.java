package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Candidacy;


public class CandidacyRepository {

  public List<Candidacy> findAll() {
    List<Candidacy> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM candidacy");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {

        Candidacy candidacy = new Candidacy(rs.getInt("id"), rs.getDate("sent_date").toLocalDate(),
            rs.getString("app_form"), rs.getString("cv"), rs.getString("cover_letter"));

        list.add(candidacy);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }



  public Candidacy findById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM candidacy WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Candidacy candidacy = new Candidacy(rs.getInt("id"), rs.getDate("sent_date").toLocalDate(),
            rs.getString("app_form"), rs.getString("cv"), rs.getString("cover_letter"));
        return candidacy;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }



  public void save(Candidacy candidacy) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO candidacy (id_session, id_applicant, sent_date, app_form, cv, cover_letter, id_status) VALUES (?,?,?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, candidacy.getSession().getId());
      stmt.setInt(2, candidacy.getApplicant().getId());
      stmt.setDate(3, java.sql.Date.valueOf(candidacy.getSentDate()));
      stmt.setString(4, candidacy.getAppForm());
      stmt.setString(5, candidacy.getCv());
      stmt.setString(6, candidacy.getCoverLetter());
      stmt.setInt(7, candidacy.getStatus().getId());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        candidacy.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }



  public boolean update(Candidacy candidacy) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE candidacy SET id_session=?, id_applicant=?, sent_date=?, app_form=?, cv=?, cover_letter=?, id_status=? WHERE id=?");

      stmt.setInt(1, candidacy.getSession().getId());
      stmt.setInt(2, candidacy.getApplicant().getId());
      stmt.setDate(3, java.sql.Date.valueOf(candidacy.getSentDate()));
      stmt.setString(4, candidacy.getAppForm());
      stmt.setString(5, candidacy.getCv());
      stmt.setString(6, candidacy.getCoverLetter());
      stmt.setInt(7, candidacy.getStatus().getId());
      stmt.setInt(8, candidacy.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM candidacy WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}

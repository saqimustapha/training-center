package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Role;

public class RoleRepository {

  public List<Role> findAll() {
    List<Role> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM role");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Role role = new Role(rs.getInt("id"), rs.getString("label"));
        list.add(role);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
  
  public Role findById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM role WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Role role = new Role(rs.getInt("id"), rs.getString("label"));

        return role;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Role role) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO role (label) VALUES (?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, role.getLabel());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        role.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Role role) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE role SET label=? WHERE id=?");

      stmt.setString(1, role.getLabel());
      stmt.setInt(2, role.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM role WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}

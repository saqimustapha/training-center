package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Status;

public class StatusRepository {

  public List<Status> findAll() {
    List<Status> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM status");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Status status = new Status(rs.getInt("id"), rs.getString("label"));
        list.add(status);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
  
  public Status findById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM status WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Status status = new Status(rs.getInt("id"), rs.getString("label"));

        return status;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Status status) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO status (label) VALUES (?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, status.getLabel());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        status.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Status status) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE status SET label=? WHERE id=?");

      stmt.setString(1, status.getLabel());
      stmt.setInt(2, status.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM status WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}

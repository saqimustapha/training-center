package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Training;

public class TrainingRepository {
  
  public List<Training> findAll() {
    List<Training> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM training");

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {

          Training training = new Training(
                  rs.getInt("id"),
                  rs.getString("name"),
                  rs.getString("description"));
                
          list.add(training);
      }

  } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
  }
  return list;
}

public void save(Training training) {
  try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("INSERT INTO training (name, description) VALUES (?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, training.getName());
      stmt.setString(2, training.getDescription());
      

      stmt.executeUpdate();
  
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {

          training.setId(rs.getInt(1));
      }

  } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
  }

}

public Training findById(int id) {
  
  try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM training WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
          Training training = new Training(
                  rs.getInt("id"),
                  rs.getString("name"),
                  rs.getString("description"));
                 
          return training;
         
      }

  } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
  }

  return null;
  
}

public boolean deleteById(int id) {
  try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM training WHERE id=?");

      stmt.setInt(1, id);

      return stmt.executeUpdate() == 1;

  } catch (SQLException e) {
      e.printStackTrace();

  }
  return false;
}

public boolean update(Training training) {
  try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("UPDATE training SET name=?,description=? WHERE id=?");
      
      stmt.setString(1,training.getName());
      stmt.setString(2, training.getDescription());
      stmt.setInt(3, training.getId());

      return stmt.executeUpdate() == 1;
      

  } catch (SQLException e) {
      e.printStackTrace();

  }
  return false;
}
}




package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Role;
import co.simplon.promo18.entity.Person;

public class PersonRepository {

  public List<Person> findAll() {
    List<Person> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Person person =
            new Person(rs.getInt("id"), rs.getString("name"), rs.getString("first_name"),
                rs.getDate("birth_date").toLocalDate(), rs.getString("phone"),
                rs.getString("email"), rs.getString("street_nbr"), rs.getString("street_name"),
                rs.getString("zipcode"), rs.getString("city"), rs.getString("country"));
        list.add(person);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Person findById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Person person =
            new Person(rs.getInt("id"), rs.getString("name"), rs.getString("first_name"),
                rs.getDate("birth_date").toLocalDate(), rs.getString("phone"),
                rs.getString("email"), rs.getString("street_nbr"), rs.getString("street_name"),
                rs.getString("zipcode"), rs.getString("city"), rs.getString("country"));

        return person;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Person person) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO person (id_role, name, first_name, birth_date, phone, email, street_nbr, street_name, zipcode, city, country) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, person.getRole().getId());
      stmt.setString(2, person.getName());
      stmt.setString(3, person.getFirstName());
      stmt.setDate(4, java.sql.Date.valueOf(person.getBrithDate()));
      stmt.setString(5, person.getPhone());
      stmt.setString(6, person.getEmail());
      stmt.setString(7, person.getStreetNbr());
      stmt.setString(8, person.getStreetName());
      stmt.setString(9, person.getZipCode());
      stmt.setString(10, person.getCity());
      stmt.setString(11, person.getCountry());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        person.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Person person) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE person SET id_role=?, name=?, first_name=?, birth_date=?, phone=?, email=?, street_nbr=?, street_name=?, zipcode=?, city=?, country=? WHERE id=?");

      stmt.setInt(1, person.getRole().getId());
      stmt.setString(2, person.getName());
      stmt.setString(3, person.getFirstName());
      stmt.setDate(4, java.sql.Date.valueOf(person.getBrithDate()));
      stmt.setString(5, person.getPhone());
      stmt.setString(6, person.getEmail());
      stmt.setString(7, person.getStreetNbr());
      stmt.setString(8, person.getStreetName());
      stmt.setString(9, person.getZipCode());
      stmt.setString(10, person.getCity());
      stmt.setString(11, person.getCountry());
      stmt.setInt(12, person.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM person WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public List<Person> findByRole(int role) {
    List<Person> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person WHERE id_role=?");

      stmt.setInt(1, role);

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Person person =
            new Person(rs.getInt("id"), rs.getString("name"), rs.getString("first_name"),
                rs.getDate("birth_date").toLocalDate(), rs.getString("phone"),
                rs.getString("email"), rs.getString("street_nbr"), rs.getString("street_name"),
                rs.getString("zipcode"), rs.getString("city"), rs.getString("country"));
        list.add(person);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Person> findByRole(Role role) {
    List<Person> list = this.findByRole(role.getId());
    for (Person person : list) {
      person.setRole(role);
    }
    return list;
  }

  public void updateRole(Person person) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT role.id, role.label FROM role INNER JOIN person ON person.id_role = role.id WHERE person.id=?");

      stmt.setInt(1, person.getId());

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        person.setRole(new Role(rs.getInt("id"),rs.getString("label")));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}

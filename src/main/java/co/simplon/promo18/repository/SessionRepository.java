package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Session;

public class SessionRepository {

  public List<Session> findAll() {
    List<Session> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM session");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {

        Session session = new Session(rs.getInt("id"), rs.getDate("start_date").toLocalDate(),
            rs.getDate("end_date").toLocalDate());
        list.add(session);
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return list;
  }

  public void save(Session session) {
    if (session.isValid()) {
      try (Connection connection = DbUtil.connect()) {
        PreparedStatement stmt = connection.prepareStatement(
            "INSERT INTO session (id_training, id_referent, start_date, end_date) VALUES (?,?,?,?)",
            PreparedStatement.RETURN_GENERATED_KEYS);

        stmt.setInt(1, session.getTraining().getId());
        stmt.setInt(2, session.getReferent().getId());
        stmt.setDate(3, java.sql.Date.valueOf(session.getStartDate()));
        stmt.setDate(4, java.sql.Date.valueOf(session.getEndDate()));

        stmt.executeUpdate();

        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {

          session.setId(rs.getInt(1));
        }

      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  public Session findById(int id) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM session WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Session session = new Session(rs.getInt("id"), rs.getDate("start_date").toLocalDate(),
            rs.getDate("end_date").toLocalDate());

        return session;

      }

    } catch (SQLException e) {

      e.printStackTrace();
    }

    return null;

  }

  public boolean deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM session WHERE id=?");

      stmt.setInt(1, id);

      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      e.printStackTrace();

    }
    return false;
  }

  public boolean update(Session session) {
    if (session.isValid()) {
      try (Connection connection = DbUtil.connect()) {
        PreparedStatement stmt = connection.prepareStatement(
            "UPDATE session SET id_training=?, id_referent=?, start_date=?, end_date=? WHERE id=?");

        stmt.setInt(1, session.getTraining().getId());
        stmt.setInt(2, session.getReferent().getId());
        stmt.setDate(3, java.sql.Date.valueOf(session.getStartDate()));
        stmt.setDate(4, java.sql.Date.valueOf(session.getEndDate()));
        stmt.setInt(5, session.getId());

        return stmt.executeUpdate() == 1;


      } catch (SQLException e) {
        e.printStackTrace();

      }
    }
    return false;
  }
}


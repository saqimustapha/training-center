package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Commentary;

public class CommentaryRepository {

  public List<Commentary> findAll() {
    List<Commentary> list = new ArrayList<>();
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commentary");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Commentary commentary = new Commentary(rs.getDate("submit_date").toLocalDate(),
            rs.getString("content"), rs.getInt("score"));

        list.add(commentary);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Commentary findById(int idCand, int idAuth) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection
          .prepareStatement("SELECT * FROM commentary WHERE id_candidacy=? AND id_author=?;");

      stmt.setInt(1, idCand);
      stmt.setInt(2, idAuth);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Commentary commentary = new Commentary(rs.getDate("submit_date").toLocalDate(),
            rs.getString("content"), rs.getInt("score"));
        return commentary;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Commentary commentary) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "REPLACE INTO commentary (id_candidacy, id_author, submit_date, content, score) VALUES (?,?,?,?,?)");

      stmt.setInt(1, commentary.getCandidacy().getId());
      stmt.setInt(2, commentary.getAuthor().getId());
      stmt.setDate(3, java.sql.Date.valueOf(commentary.getSubmitDate()));
      stmt.setString(4, commentary.getContent());
      stmt.setInt(5, commentary.getScore());

      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Commentary commentary) {

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE commentary SET submit_date=?, content=?, score=? WHERE id_candidacy=? AND id_author=?");

      stmt.setDate(1, java.sql.Date.valueOf(commentary.getSubmitDate()));
      stmt.setString(2, commentary.getContent());
      stmt.setInt(3, commentary.getScore());
      stmt.setInt(4, commentary.getCandidacy().getId());
      stmt.setInt(5, commentary.getAuthor().getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int idCand, int idAuth) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM commentary WHERE id_candidacy=? AND id_author=?");

      stmt.setInt(1, idCand);
      stmt.setInt(2, idAuth);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}

package co.simplon.promo18.entity;

import java.time.LocalDate;
import java.util.List;

public class Candidacy {
  private Integer id;
  private Session session;
  private Person applicant;
  private LocalDate sentDate;
  private String appForm;
  private String cv;
  private String coverLetter;
  private Status status;
  private List<Commentary> commentaries;

  public Candidacy() {}

  public Candidacy(Integer id, LocalDate sentDate, String appForm, String cv, String coverLetter) {
    this.id = id;
    this.sentDate = sentDate;
    this.appForm = appForm;
    this.cv = cv;
    this.coverLetter = coverLetter;
  }

  public Candidacy(Session session, Person applicant, LocalDate sentDate, String appForm, String cv,
      String coverLetter, Status status, List<Commentary> commentaries) {
    this.session = session;
    this.applicant = applicant;
    this.sentDate = sentDate;
    this.appForm = appForm;
    this.cv = cv;
    this.coverLetter = coverLetter;
    this.status = status;
    this.commentaries = commentaries;
  }

  public Candidacy(Integer id, Session session, Person applicant, LocalDate sentDate,
      String appForm, String cv, String coverLetter, Status status, List<Commentary> commentaries) {
    this.id = id;
    this.session = session;
    this.applicant = applicant;
    this.sentDate = sentDate;
    this.appForm = appForm;
    this.cv = cv;
    this.coverLetter = coverLetter;
    this.status = status;
    this.commentaries = commentaries;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Session getSession() {
    return session;
  }

  public void setSession(Session session) {
    this.session = session;
  }

  public Person getApplicant() {
    return applicant;
  }

  public void setApplicant(Person applicant) {
    this.applicant = applicant;
  }

  public LocalDate getSentDate() {
    return sentDate;
  }

  public void setSentDate(LocalDate sentDate) {
    this.sentDate = sentDate;
  }

  public String getAppForm() {
    return appForm;
  }

  public void setAppForm(String appForm) {
    this.appForm = appForm;
  }

  public String getCv() {
    return cv;
  }

  public void setCv(String cv) {
    this.cv = cv;
  }

  public String getCoverLetter() {
    return coverLetter;
  }

  public void setCoverLetter(String coverLetter) {
    this.coverLetter = coverLetter;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public List<Commentary> getCommentaries() {
    return commentaries;
  }

  public void setCommentaries(List<Commentary> commentaries) {
    this.commentaries = commentaries;
  }

  @Override
  public String toString() {
    return "Candidacy [appForm=" + appForm + ", applicant=" + applicant + ", commentaries="
        + commentaries + ", coverLetter=" + coverLetter + ", cv=" + cv + ", id=" + id
        + ", sentDate=" + sentDate + ", session=" + session + ", status=" + status + "]";
  }


}

package co.simplon.promo18.entity;

import java.time.LocalDate;
import java.util.List;

public class Session {
  private Integer id;
  private Training training;
  private LocalDate startDate;
  private LocalDate endDate;
  private Person referent;
  private List<Person> formers;
  private List<Candidacy> candidacies;

  public Session(Integer id, LocalDate startDate, LocalDate endDate) {
    this.id = id;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public Session() {}

  public Session(Training training, LocalDate startDate, LocalDate endDate, Person referent) {
    this.training = training;
    this.startDate = startDate;
    this.endDate = endDate;
    this.referent = referent;
  }

  public Session(Integer id, Training training, LocalDate startDate, LocalDate endDate,
      Person referent) {
    this.id = id;
    this.training = training;
    this.startDate = startDate;
    this.endDate = endDate;
    this.referent = referent;
  }

  public Session(Training training, LocalDate startDate, LocalDate endDate, Person referent,
      List<Person> formers, List<Candidacy> candidacies) {
    this.training = training;
    this.startDate = startDate;
    this.endDate = endDate;
    this.referent = referent;
    this.formers = formers;
    this.candidacies = candidacies;
  }

  public Session(Integer id, Training training, LocalDate startDate, LocalDate endDate,
      Person referent, List<Person> formers, List<Candidacy> candidacies) {
    this.id = id;
    this.training = training;
    this.startDate = startDate;
    this.endDate = endDate;
    this.referent = referent;
    this.formers = formers;
    this.candidacies = candidacies;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Training getTraining() {
    return training;
  }

  public void setTraining(Training training) {
    this.training = training;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public Person getReferent() {
    return referent;
  }

  public void setReferent(Person referent) {
    this.referent = referent;
  }

  public List<Person> getFormer() {
    return formers;
  }

  public void setFormer(List<Person> former) {
    this.formers = former;
  }

  public List<Candidacy> getCandidacy() {
    return candidacies;
  }

  public void setCandidacy(List<Candidacy> candidacy) {
    this.candidacies = candidacy;
  }

  public boolean isValid() {
    boolean valid = false;
    if (this.training != null && endDate.compareTo(startDate) > 0 && referent != null) {
      valid = true;
    }
    return valid;
  }

  @Override
  public String toString() {
    return "Session [candidacy=" + candidacies + ", endDate=" + endDate + ", former=" + formers
        + ", id=" + id + ", referent=" + referent + ", startDate=" + startDate + ", training="
        + training + "]";
  }
}

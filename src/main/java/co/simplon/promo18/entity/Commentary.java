package co.simplon.promo18.entity;

import java.time.LocalDate;

public class Commentary {
  private Candidacy candidacy;
  private Person author;
  private LocalDate submitDate;
  private String content;
  private Integer score;

  public Commentary() {}

  public Commentary(LocalDate submitDate, String content, Integer score) {
    this.submitDate = submitDate;
    this.content = content;
    this.score = score;
  }

  public Commentary(Candidacy candidacy, LocalDate submitDate, String content, Integer score) {
    this.candidacy = candidacy;
    this.submitDate = submitDate;
    this.content = content;
    this.score = score;
  }

  public Commentary(Person author, LocalDate submitDate, String content, Integer score) {
    this.author = author;
    this.submitDate = submitDate;
    this.content = content;
    this.score = score;
  }

  public Commentary(Candidacy candidacy, Person author, LocalDate submitDate, String content,
      Integer score) {
    this.candidacy = candidacy;
    this.author = author;
    this.submitDate = submitDate;
    this.content = content;
    this.score = score;
  }

  public Candidacy getCandidacy() {
    return candidacy;
  }

  public void setCandidacy(Candidacy candidacy) {
    this.candidacy = candidacy;
  }

  public Person getAuthor() {
    return author;
  }

  public void setAuthor(Person author) {
    this.author = author;
  }

  public LocalDate getSubmitDate() {
    return submitDate;
  }

  public void setSubmitDate(LocalDate submitDate) {
    this.submitDate = submitDate;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Integer getScore() {
    return score;
  }

  public void setScore(Integer score) {
    this.score = score;
  }

  @Override
  public String toString() {
    return "commentary [author=" + author + ", candidacy=" + candidacy + ", content=" + content
        + ", score=" + score + ", submitDate=" + submitDate + "]";
  }
}

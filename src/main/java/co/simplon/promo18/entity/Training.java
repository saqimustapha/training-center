package co.simplon.promo18.entity;

public class Training {
  private Integer id;
  private String name;
  private String description;

  public Training() {}

  public Training(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public Training(Integer id, String name, String description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  public Integer getId() {
    return id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Training [description=" + description + ", id=" + id + ", name=" + name + "]";
  }
}

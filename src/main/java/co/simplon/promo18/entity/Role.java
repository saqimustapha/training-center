package co.simplon.promo18.entity;

public class Role {
  private Integer id;
  private String label;

  public Role() {}

  public Role(String label) {
    this.label = label;
  }

  public Role(Integer id, String label) {
    this.id = id;
    this.label = label;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return "Role [id=" + id + ", label=" + label + "]";
  }
}

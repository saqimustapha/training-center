package co.simplon.promo18.entity;

import java.time.LocalDate;

public class Person {
  private Integer id;
  private Role role;
  private String name;
  private String firstName;
  private LocalDate brithDate;
  private String phone;
  private String email;
  private String streetNbr;
  private String streetName;
  private String zipCode;
  private String city;
  private String country;

  public Person() {}

  public Person(Integer id, String name, String firstName, LocalDate brithDate, String phone,
      String email, String streetNbr, String streetName, String zipCode, String city,
      String country) {
    this.id = id;
    this.name = name;
    this.firstName = firstName;
    this.brithDate = brithDate;
    this.phone = phone;
    this.email = email;
    this.streetNbr = streetNbr;
    this.streetName = streetName;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
  }

  public Person(Role role, String name, String firstName, LocalDate brithDate, String phone,
      String email, String streetNbr, String streetName, String zipCode, String city,
      String country) {
    this.role = role;
    this.name = name;
    this.firstName = firstName;
    this.brithDate = brithDate;
    this.phone = phone;
    this.email = email;
    this.streetNbr = streetNbr;
    this.streetName = streetName;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
  }

  public Person(Integer id, Role role, String name, String firstName, LocalDate brithDate,
      String phone, String email, String streetNbr, String streetName, String zipCode, String city,
      String country) {
    this.id = id;
    this.role = role;
    this.name = name;
    this.firstName = firstName;
    this.brithDate = brithDate;
    this.phone = phone;
    this.email = email;
    this.streetNbr = streetNbr;
    this.streetName = streetName;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public LocalDate getBrithDate() {
    return brithDate;
  }

  public void setBrithDate(LocalDate brithDate) {
    this.brithDate = brithDate;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getStreetNbr() {
    return streetNbr;
  }

  public void setStreetNbr(String streetNbr) {
    this.streetNbr = streetNbr;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Override
  public String toString() {
    return "Person [brithDate=" + brithDate + ", city=" + city + ", country=" + country + ", email="
        + email + ", firstName=" + firstName + ", id=" + id + ", name=" + name + ", phone=" + phone
        + ", role=" + role + ", streetName=" + streetName + ", streetNbr=" + streetNbr
        + ", zipCode=" + zipCode + "]";
  }
}

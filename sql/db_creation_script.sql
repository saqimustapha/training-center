CREATE TABLE role (
  id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(32) NOT NULL
);

INSERT INTO
  role (label)
VALUES
  ('Candidat'),
  ('Apprenant'),
  ('Formateur'),
  ('Responsable Formation');

CREATE TABLE training (
  id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  description VARCHAR(255) NOT NULL
);

INSERT INTO
  training (name, description)
VALUES
  (
    "Développeur web et web mobile",
    "Développer la partie front-end et la partie back-end d’une application web ou web mobile en intégrant les recommandations de sécurité"
  ),
  (
    "Concepteur développeur d'applications",
    "Concevoir et développer une application multicouche répartie en intégrant les recommandations de sécurité"
  );

CREATE TABLE status (
  id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(32) NOT NULL
);

INSERT INTO
  status (label)
VALUES
  ('Déposée'),
  ('En attente'),
  ('Acceptée'),
  ('Refusée'),
  ('Confirmée');

CREATE TABLE person (
  id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  id_role TINYINT UNSIGNED,
  name VARCHAR(64) NOT NULL,
  first_name VARCHAR(64) NOT NULL,
  birth_date DATE NOT NULL,
  phone VARCHAR(32) NOT NULL,
  email VARCHAR(128) NOT NULL,
  street_nbr VARCHAR(16) NOT NULL,
  street_name VARCHAR(128) NOT NULL,
  zipcode CHAR(5) NOT NULL,
  city VARCHAR(64) NOT NULL,
  country VARCHAR(64) NOT NULL,
  CONSTRAINT FK_person_role FOREIGN KEY (id_role) REFERENCES role(id) ON DELETE
  SET
    NULL
);

INSERT INTO
  person (
    id_role,
    name,
    first_name,
    birth_date,
    phone,
    email,
    street_nbr,
    street_name,
    zipcode,
    city,
    country
  )
VALUES
  (
    1,
    'Lereau',
    'Amélie',
    '1996-12-03',
    '+33 4 82 96 33 48',
    'amelielereau@dayrep.com',
    '59',
    'rue de la République',
    '69002',
    'Lyon',
    'France'
  ),
  (
    1,
    'Morneau',
    'Durandana',
    '1986-08-07',
    '+33',
    'DurandanaMorneau@jourrapide.com',
    '81',
    'avenue De Marlioz',
    '74100',
    'Annemasse',
    'France'
  ),
  (
    1,
    'Vadeboncoeur',
    'Raymond',
    '1992-01-24',
    '+33 4 24 09 25 63',
    'raymondvadeboncoeur@teleworm.us',
    '43',
    'place du Jeu de Paume',
    '69400',
    'Villefranche-sur-Saône',
    'France'
  ),
  (
    1,
    'Bondy',
    'Émile',
    '1985-02-14',
    '+33 4 94 49 54 97',
    'emilebondy@jourrapide.com',
    '13',
    'rue Banaudon',
    '69006',
    'Lyon',
    'France'
  ),
  (
    1,
    'Monrency',
    'Ignace',
    '1995-09-15',
    '+33 4 79 49 88 97',
    'ignacemonrency@teleworm.us',
    '45',
    'rue de Groussay',
    '26100',
    'Romans-sur-isère',
    'France'
  ),
  (
    1,
    'Desrosiers',
    'Ambra',
    '1979-11-04',
    '+33 4 48 26 02 42',
    'ambradesrosiers@dayrep.com',
    '80',
    'avenue Millies Lacroix',
    '38130',
    'Échirolles',
    'France'
  ),
  (
    1,
    'Corbeil',
    'Eliot',
    '1978-08-01',
    '+33 4 86 04 10 29',
    'eliotcorbeil@jourrapide.com ',
    '97',
    'rue Gustave Eiffel',
    '69140',
    'Rillieux-la-Pape',
    'France'
  ),
  (
    1,
    'Labossière',
    'Arlette',
    '1997-10-14',
    '+33 4 31 84 51 10',
    'arlettelabossiere@jourrapide.com',
    '95',
    'boulevard Albin Durand',
    '73000',
    'Chambéry',
    'France'
  ),
  (
    1,
    'Guimond',
    'Élisabeth',
    ' 1988-02-28',
    '+33 4 60 97 20 51',
    'elisabethguimond@jourrapide.com',
    '35',
    'boulevard d Alsace',
    '69200',
    'Vénissieux',
    'France'
  ),
  (
    1,
    'Donat',
    'René',
    '1984-07-09',
    '+33 4 26 03 29 74',
    'renedonat@armyspy.com',
    '11',
    'rue Banaudon',
    '69005',
    'Lyon',
    'France'
  ),
  (
    2,
    'Trudeau',
    'Astolpho',
    '1984-12-30',
    '+33 4 56 92 80 16',
    'astolphotrudeau@teleworm.us',
    '6',
    'boulevard de Normandie',
    '38600',
    'Fontaine',
    'France'
  ),
  (
    3,
    'Langlais',
    'Damiane',
    '1986-09-16',
    '+33 4 51 40 72 88',
    'damianelanglais@armyspy.com',
    '22',
    'avenue Millies Lacroix',
    '38130',
    'Échirolles',
    'France'
  ),
  (
    3,
    'Turgeon',
    'Melodie',
    '1992-05-18',
    '+33  4 84 74 81 83',
    'melodiet urgeon@teleworm.us',
    '99',
    'boulevard Amiral Courbet',
    '69600',
    'Oullins',
    'France'
  ),
  (
    3,
    'Boisvert',
    'Anton',
    '1985-06-20',
    '+33 4 53 93 91 52',
    'antonboisvert@teleworm.us',
    '33',
    'place du Jeu de Paume',
    '38200',
    'Vienne',
    'France'
  ),
  (
    4,
    'Maheu',
    'Bruno',
    '1971-12-14',
    '+33 4 03 92 19 89',
    'brunomaheu@armyspy.com ',
    '6',
    'place de l Hôtel de ville',
    '38200',
    'Vienne',
    'France'
  ),
  (
    4,
    'Beauchemin',
    'Élise',
    '1969-03-27',
    '+33 4 16 58 94 65',
    'elisebeauchemin@rhyta.com',
    '83',
    'rue Ampère',
    '69140',
    'Rillieux-la-Pape',
    'France'
  );

CREATE TABLE session (
  id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  id_training TINYINT UNSIGNED NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  id_referent SMALLINT UNSIGNED,
  CONSTRAINT FK_session_training FOREIGN KEY (id_training) REFERENCES training(id) ON DELETE CASCADE,
  CONSTRAINT FK_session_person FOREIGN KEY (id_referent) REFERENCES person(id) ON DELETE
  SET
    NULL
);

INSERT INTO
  session (id_training, start_date, end_date, id_referent)
VALUES
  (1, '2022-04-04', '2022-07-03', 15),
  (2, '2022-06-15', '2022-10-11', 15),
  (2, '2022-09-02', '2022-05-05', 16);

CREATE TABLE session_person_joint (
  id_session SMALLINT UNSIGNED NOT NULL,
  id_person SMALLINT UNSIGNED NOT NULL,
  CONSTRAINT PK_session_person PRIMARY KEY (id_session, id_person),
  CONSTRAINT FK_sessionperson_session FOREIGN KEY (id_session) REFERENCES session(id) ON DELETE CASCADE,
  CONSTRAINT FK_sessionperson_person FOREIGN KEY (id_person) REFERENCES person(id) ON DELETE CASCADE
);

INSERT INTO
  session_person_joint (id_session, id_person)
VALUES
  (1, 12),
  (1, 13),
  (2, 13),
  (2, 14),
  (3, 14),
  (3, 12);

CREATE TABLE candidacy (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  id_session SMALLINT UNSIGNED NOT NULL,
  id_applicant SMALLINT UNSIGNED NOT NULL,
  sent_date DATE NOT NULL,
  app_form VARCHAR(255),
  cv VARCHAR(255),
  cover_letter VARCHAR(255),
  id_status TINYINT UNSIGNED,
  CONSTRAINT FK_candidacy_session FOREIGN KEY(id_session) REFERENCES session(id) ON DELETE CASCADE,
  CONSTRAINT FK_candidacy_applicant FOREIGN KEY(id_applicant) REFERENCES person(id) ON DELETE CASCADE,
  CONSTRAINT FK_candidacy_status FOREIGN KEY(id_status) REFERENCES status(id) ON DELETE
  SET
    NULL
);

INSERT INTO
  candidacy (
    id_session,
    id_applicant,
    sent_date,
    app_form,
    cv,
    cover_letter,
    id_status
  )
VALUES
  (
    1,
    1,
    '2022-01-24',
    'app_form_link_1',
    'cv_link_2',
    'cover_letter_link_3',
    1
  ),
  (
    2,
    2,
    '2022-02-23',
    'app_form_link_4',
    'cv_link_5',
    'cover_letter_link_6',
    2
  ),
  (
    3,
    3,
    '2022-03-22',
    'app_form_link_7',
    'cv_link_8',
    'cover_letter_link_9',
    3
  ),
  (
    1,
    4,
    '2022-01-21',
    'app_form_link_10',
    'cv_link_11',
    'cover_letter_link_12',
    4
  ),
  (
    2,
    5,
    '2022-02-20',
    'app_form_link_13',
    'cv_link_14',
    'cover_letter_link_15',
    1
  ),
  (
    3,
    6,
    '2022-03-19',
    'app_form_link_16',
    'cv_link_17',
    'cover_letter_link_18',
    2
  ),
  (
    1,
    7,
    '2022-01-18',
    'app_form_link_19',
    'cv_link_20',
    'cover_letter_link_21',
    3
  ),
  (
    2,
    8,
    '2022-02-17',
    'app_form_link_22',
    'cv_link_23',
    'cover_letter_link_24',
    4
  ),
  (
    3,
    9,
    '2022-03-16',
    'app_form_link_25',
    'cv_link_26',
    'cover_letter_link_28',
    1
  ),
  (
    1,
    10,
    '2022-01-15',
    'app_form_link_29',
    'cv_link_30',
    'cover_letter_link_31',
    2
  ),
  (
    2,
    11,
    '2022-02-14',
    'app_form_link_32',
    'cv_link_33',
    'cover_letter_link_34',
    5
  ),
  (
    2,
    1,
    '2022-03-13',
    'app_form_link_35',
    'cv_link_36',
    'cover_letter_link_37',
    3
  ),
  (
    3,
    1,
    '2022-01-12',
    'app_form_link_38',
    'cv_link_39',
    'cover_letter_link_40',
    4
  ),
  (
    2,
    4,
    '2022-02-11',
    'app_form_link_41',
    'cv_link_42',
    'cover_letter_link_43',
    1
  ),
  (
    3,
    8,
    '2022-03-10',
    'app_form_link_44',
    'cv_link_45',
    'cover_letter_link_46',
    2
  );

CREATE TABLE commentary (
  id_candidacy INT UNSIGNED NOT NULL,
  id_author SMALLINT UNSIGNED,
  submit_date DATE NOT NULL,
  content VARCHAR(255) NOT NULL,
  score TINYINT NOT NULL,
  CONSTRAINT PK_commentary PRIMARY KEY (id_candidacy, id_author),
  CONSTRAINT FK_commentary_candidacy FOREIGN KEY (id_candidacy) REFERENCES candidacy(id) ON DELETE CASCADE,
  CONSTRAINT FK_commentary_person FOREIGN KEY (id_author) REFERENCES person(id) ON DELETE CASCADE
);

INSERT INTO
  commentary (
    id_candidacy,
    id_author,
    submit_date,
    content,
    score
  )
VALUES
  (1, 12, '2022-03-25', 'Comment_1', 1),
  (1, 13, '2022-02-26', 'Comment_2', 2),
  (2, 14, '2022-01-24', 'Comment_3', 3),
  (2, 15, '2022-03-24', 'Comment_4', 4),
  (3, 16, '2022-02-24', 'Comment_5', 5),
  (3, 12, '2022-01-24', 'Comment_6', 1),
  (4, 13, '2022-03-24', 'Comment_7', 2),
  (5, 14, '2022-02-24', 'Comment_8', 3),
  (5, 15, '2022-01-24', 'Comment_9', 4),
  (5, 16, '2022-03-24', 'Comment_10', 5),
  (6, 12, '2022-02-24', 'Comment_11', 1),
  (7, 13, '2022-01-24', 'Comment_12', 2),
  (8, 14, '2022-03-24', 'Comment_13', 3),
  (8, 15, '2022-02-24', 'Comment_14', 4),
  (9, 16, '2022-01-24', 'Comment_15', 5),
  (9, 12, '2022-03-24', 'Comment_16', 1),
  (11, 13, '2022-02-24', 'Comment_17', 2),
  (12, 14, '2022-01-24', 'Comment_18', 3),
  (13, 15, '2022-03-24', 'Comment_19', 4),
  (14, 16, '2022-02-24', 'Comment_20', 5);